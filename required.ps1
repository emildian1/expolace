# Check if the script is run as administrator
$isAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

Write-Host "Checking admin..." -ForegroundColor Yellow
if (-not $isAdmin) {
    Write-Host "This script requires administrator privileges. Please run the script as an administrator." -ForegroundColor Yellow
    return
}

# Check if Chocolatey is installed and install it if not
Write-Host "Checking choco..." -ForegroundColor Yellow
if (-not (Get-Command choco -ErrorAction SilentlyContinue)) {
    Write-Host "Chocolatey is not installed. Installing Chocolatey..." -ForegroundColor Yellow
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    Set-ExecutionPolicy Restricted -Scope Process -Force
    Write-Host "Chocolatey installed successfully." -ForegroundColor Yellow
}

# Install programs using Chocolatey
if ((Get-Command choco -ErrorAction SilentlyContinue)) {
    Write-Host "Installing programs..." -ForegroundColor Yellow
    choco install 7zip directx dotnet-6.0-desktopruntime python3 temurin8jre temurin17jre vcredist-all xna zerotier-one -y
    Write-Host "Programs installed successfully." -ForegroundColor Green
} else {
    Write-Host "Something went wrong. Chocolatey is not properly installed or programs installed failed." -ForegroundColor Red
}
