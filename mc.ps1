# Define the URL of the .zip file and the local directory and filename where the file should be saved.
$url = "https://github.com/PrismLauncher/PrismLauncher/releases/download/7.2/PrismLauncher-Windows-MSVC-Portable-7.2.zip"
$dir = "$env:USERPROFILE\\_Temp"
$file = "$dir\\prismlauncher.zip"

# Check if the directory exists.
if (-not (Test-Path -Path $dir)) {
    # If the directory does not exist, create it.
    New-Item -ItemType Directory -Path $dir
}

# Use the Invoke-WebRequest cmdlet to download the .zip file.
Invoke-WebRequest -Uri $url -OutFile $file

# Define the path of the folder where the .zip file should be extracted.
$folder = "$env:USERPROFILE\\_Games\\Prism Launcher\\"

# Check if the directory exists.
if (-not (Test-Path -Path $dir)) {
    # If the directory does not exist, create it.
    New-Item -ItemType Directory -Path $dir
}

# Use the Expand-Archive cmdlet to extract the .zip file.
Expand-Archive -Path $file -DestinationPath $folder -Force

# Delete the original .zip file.
Remove-Item -Path $file

# Define the path of the new file to be added.
$newFile = "$folder\\accounts.json"

# Define the content to be added.
$content = '{"accounts":[{"entitlement":{"ownsMinecraft":true,"canPlayMinecraft":true},"type":"Offline"}],"formatVersion":3}'

# Use the Add-Content cmdlet to create the new file and add content to it.
Add-Content -Path $newFile -Value $content

# Define the path of the file for which the shortcut should be created.
$targetFile = "$folder\\prismlauncher.exe"

# Define the path of the shortcut to be created on the desktop.
$shortcutFile = [Environment]::GetFolderPath("Desktop") + "\\Start Minecraft.lnk"

# Create a WScript Shell object.
$WScriptShell = New-Object -ComObject WScript.Shell

# Create the shortcut.
$shortcut = $WScriptShell.CreateShortcut($shortcutFile)

# Set the target path of the shortcut.
$shortcut.TargetPath = $targetFile

# Save the shortcut.
$shortcut.Save()

Copy-Item -Path $shortcutFile -Destination "$env:APPDATA\\Microsoft\\Windows\\Start Menu\\Programs"
